var graph = [];


var searched = [];
var search_queue = [];

function setup() {
    createCanvas(640,480);

    graph['you'] = [
    'alice',
    'bob',
    'claire'
    ];

    graph['alice'] = ['peggy'];
    graph['bob'] = ['anuj', 'peggy'];
    graph['claire'] = ['thom', 'jonnie',];
    graph['anuj'] = [];
    graph['peggy'] = [];
    graph['thom'] = [];
    graph['jonnie'] = [];

    var p = breadth_search('you');
    console.log(p);
    //graph.splice(0, 1);
}

function draw() {
    background(0);

       
}

function isMangoSeller(nm) {
    return nm.slice(-1) == 'e';
}

function breadth_search(nm) {
    search_queue = graph[nm];
    delete graph['you'];
    
    while (search_queue.length > 0) {
        var person = search_queue.shift();
        
        if (searched.indexOf(person) == -1) {
            //console.log(person);
            if (isMangoSeller(person)) {
                return person;
            } else {
                search_queue = search_queue.concat(graph[person]);
                searched.push(person);

                //console.log(searched);

            }
        } else {
            console.log(person + ' already searched');
        }
    }

    return false;
}