/*
binary search: 
verwacht een array met gesorteerde items
je wil een item zoeken in de array

bvb nr tussen 0 en 10: (juiste is 4):
guess 5: te hoog, dus alles tss 0 en 5 kan al niet meer
nu helft van 0 en 4 nemen => = guess 2 => te klein
nu helft van 7 nemen (3 (2+1) + 4) => 3,5 afgerond = guess 4
*/
function binary_search(list, item) {
    var low = 0;
    var high = list.length-1;

    console.log('expected result:', item);
    var nr_iterations = 0;
    while(low <= high) {
        nr_iterations++;
        var mid = (low+high)/2;
        mid = Math.ceil(mid);
        var guess = list[mid];
        console.log('low, high: ', low, high);
        console.log('guess:', guess);
        console.log('mid:', mid);
        if (guess == item) {
            console.log('nr_iterations=' + nr_iterations);
            return guess;
        }

        if (guess > item) {
            high = mid - 1;
        } else {
            low = mid + 1;
        }
    }

    console.log('nr_iterations=' + nr_iterations);
    console.log('jaa');
    return null;
}

var arr = [0,1,2,3,4,5,6,7,8,9,10];
var ret = binary_search(arr, 4);
console.log('result='+ret);